﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider systemDataProvider = new SystemDataProvider();
            systemDataProvider.Attach(new ConsoleLogger());
            systemDataProvider.Attach(new FileLogger("SystemData.txt"));
            while (true)
            {
                systemDataProvider.GetCPULoad();
                systemDataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}

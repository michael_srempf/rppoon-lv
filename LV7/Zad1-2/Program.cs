﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zad 1
            Double[] array = new double[] { 5, 9, 11, 1, 7, 3, 8, 22, 2, 4, 16 };
            NumberSequence numberSequence1 = new NumberSequence(array);
            numberSequence1.SetSortStrategy(new SequentialSort());
            numberSequence1.Sort();
            Console.WriteLine("Sequential sort:");
            Console.WriteLine(numberSequence1.ToString());

            NumberSequence numberSequence2 = new NumberSequence(array);
            numberSequence2.SetSortStrategy(new BubbleSort());
            numberSequence2.Sort();
            Console.WriteLine("Bubble sort:");
            Console.WriteLine(numberSequence2.ToString());

            NumberSequence numberSequence3 = new NumberSequence(array);
            numberSequence3.SetSortStrategy(new BubbleSort());
            numberSequence3.Sort();
            Console.WriteLine("Comb sort:");
            Console.WriteLine(numberSequence3.ToString());

            //Zad 2
            NumberSequence numberSequence4 = new NumberSequence(array);
            
            numberSequence4.SetSearchStrategy(new LinearSearch());
            int indexOfSearch = numberSequence4.Search(11);
            if (indexOfSearch >= 0 ) Console.WriteLine("Number 11 is contained in array at index "+indexOfSearch);
            else Console.WriteLine("Number 11 isn't contained in array");
            indexOfSearch = numberSequence4.Search(6);
            if (indexOfSearch >= 0) Console.WriteLine("Number 6 is contained in array at index"+indexOfSearch);
            else Console.WriteLine("Number 6 isn't contained in array");
            
        }
    }
}

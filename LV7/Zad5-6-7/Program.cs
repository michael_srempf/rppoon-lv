﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5_6_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zad 5
            Console.WriteLine("Buying:");
            DVD movie = new DVD("Harry Potter", DVDType.MOVIE, 19.99);
            DVD software = new DVD("Microsoft Word", DVDType.SOFTWARE, 61.99);
            VHS vhsMovie = new VHS("Jurassic Park", 41.99);
            Book book = new Book("Lord of The Rings", 15.99);
            BuyVisitor buyVisitor = new BuyVisitor();
            Console.WriteLine(movie.ToString());
            Console.WriteLine("DVD with tax is: " + movie.Accept(buyVisitor));
            Console.WriteLine(software.ToString());
            Console.WriteLine("DVD with tax is: " + software.Accept(buyVisitor));
            Console.WriteLine(vhsMovie.ToString());
            Console.WriteLine("VHS with tax is: " + vhsMovie.Accept(buyVisitor));
            Console.WriteLine(book.ToString());
            Console.WriteLine("Book with tax is: " + book.Accept(buyVisitor));

            //Zad 6
            Console.WriteLine("\n\n\nRenting:");
            RentVisitor rentVisitor = new RentVisitor();
            Console.WriteLine(movie.ToString());
            Console.WriteLine("DVD with tax is: " + movie.Accept(rentVisitor));
            Console.WriteLine(software.ToString());
            Console.WriteLine("DVD with tax is: " + software.Accept(rentVisitor));
            Console.WriteLine(vhsMovie.ToString());
            Console.WriteLine("VHS with tax is: " + vhsMovie.Accept(rentVisitor));
            Console.WriteLine(book.ToString());
            Console.WriteLine("Book with tax is: " + book.Accept(rentVisitor));

            //Zad 7
            Console.WriteLine("\n\n\nCart:");
            RentVisitorV2 rentVisitorV2 = new RentVisitorV2();
            Cart cart = new Cart();
            cart.Add(movie);
            cart.Add(book);
            cart.Add(software);
            cart.Add(vhsMovie);
            Console.WriteLine("Price if you were to buy all items: " + cart.Accept(buyVisitor));
            Console.WriteLine("Price if you were to rent all rentable items: " + cart.Accept(rentVisitorV2));
        }
    }
}

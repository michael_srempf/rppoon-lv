﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5_6_7
{
    //Zad 7 verzira RentVisitor za zad 7
    class RentVisitorV2 : IVisitor
    {
        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type != DVDType.SOFTWARE)
            {
                return DVDItem.Price * 0.1;
            }
            return DVDItem.Price;
        }

        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * 0.1;
        }

        public double Visit(Book BookItem)
        {
            return BookItem.Price * 0.1;
        }
    }
}

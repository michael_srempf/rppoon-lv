﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5_6_7
{
    class Cart:IItem
    {
        private List<IItem> items;
        public Cart()
        {
            this.items = new List<IItem>();
        }

        public double Accept(IVisitor visitor)
        {
            double price = 0;
            foreach(IItem item in items)
            {
                price += item.Accept(visitor);
            }
            return price;
        }

        public void Add(IItem item)
        {
            this.items.Add(item);
        }
        public void Remove(IItem item)
        {
            this.items.Remove(item);
        }

    }
}

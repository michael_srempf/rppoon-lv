﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (((currentLoad /this.previousCPULoad > 1.1) || (currentLoad / this.previousCPULoad < 0.9))&& (currentLoad!=this.previousCPULoad))
            {
                
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            float currentAvailableRAM = this.AvailableRAM;
            if (((currentAvailableRAM /this.previousRAMAvailable > 1.1) || (currentAvailableRAM / this.previousRAMAvailable < 0.9))&&(currentAvailableRAM!=this.previousRAMAvailable))
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentAvailableRAM;
            return currentAvailableRAM;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6_7
{
    class EmailValidator : IEmailValidatorService
    {
        public bool IsValidAddress(string candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return (ContainsRequiredSigns(candidate)&&StartsWithName(candidate));
        }
        private bool StartsWithName(string candidate)
        {
            bool StartsWithName = true;
            if (candidate.StartsWith("@"))
            {
                StartsWithName = false;
            }
            return StartsWithName;
        }
        private bool ContainsRequiredSigns(string candidate)
        {
            
            bool hasCom = false, hasHr = false, hasAt = false;
            if (candidate.Contains(".com")) 
            {
                hasCom = true;
            }
            if (candidate.Contains(".hr")) 
            {
                hasHr = true;
            }
            if (candidate.Contains("@")) 
            {
                hasAt = true;
            } 
            return ((hasCom || hasHr) && hasAt);
        }
    }
}

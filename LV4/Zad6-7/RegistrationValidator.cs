﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6_7
{
    class RegistrationValidator : IRegistrationValidator
    {
        private PasswordValidator passwordValidator;
        private EmailValidator emailValidator;
        public RegistrationValidator(int PasswordLenght) 
        {
            this.passwordValidator = new PasswordValidator(PasswordLenght);
            this.emailValidator = new EmailValidator();
        }
        public bool IsUserEntryValid(UserEntry entry)
        {
            return (emailValidator.IsValidAddress(entry.Email) && passwordValidator.IsValidPassword(entry.Password));
        }
    }
}

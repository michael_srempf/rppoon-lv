﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3_4_5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 3
            List<IRentable> items = new List<IRentable>();
            items.Add(new Video("The Godfather"));
            items.Add(new Book("Harry Potter and the goblet of fire"));
            RentingConsolePrinter printer = new RentingConsolePrinter();
            Console.WriteLine("Items on list are:");
            printer.DisplayItems(items);
            Console.WriteLine("Total price is:");
            printer.PrintTotalPrice(items);


            //Zadatak 4 - razlika je u cijeni zato što hotiitem ima veću cijenu za 1.99 po itemu
            Console.WriteLine("\n\nList with hot items and regular items:");
            List<IRentable> items2 = new List<IRentable>();
            items2.Add(new Video("The Godfather"));
            items2.Add(new Book("Harry Potter and the goblet of fire"));
            items2.Add(new HotItem(new Book("The Lord of the Rings: The Two Towers")));
            items2.Add(new HotItem(new Video("Avengers: Endgame")));
            Console.WriteLine("Items on list are:");
            printer.DisplayItems(items2);
            Console.WriteLine("Total price is:");
            printer.PrintTotalPrice(items2);

            //Zadatak 5
            Console.WriteLine("\n\nItems on sale:");
            List<IRentable> flashSale = new List<IRentable>();
            foreach(IRentable item in items2)
            {
                flashSale.Add(new DiscountedItem(item,20));
            }
            Console.WriteLine("Items on list are:");
            printer.DisplayItems(flashSale);
            Console.WriteLine("Total price is:");
            printer.PrintTotalPrice(flashSale);

        }
    }
}

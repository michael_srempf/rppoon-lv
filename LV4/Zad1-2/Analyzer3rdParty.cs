﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1_2
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            double[] results = new double[data[0].Length];
            for (int i = 0; i < data[0].Length; i++)
            {
                for(int j=0;j<data.Length;j++)
                {
                    results[i] += data[j][i];
                }
                results[i] /= data.Length;
                
            }
            return results;
        }
    }
}

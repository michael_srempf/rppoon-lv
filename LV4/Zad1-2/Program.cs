﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            Dataset dataset = new Dataset("test.csv");
            foreach (List<double> row in dataset.GetData())
            {
                foreach (double data in row)
                {
                    Console.Write(data + "\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Average per column is:");
            double[] AveragePerColumn = adapter.CalculateAveragePerColumn(dataset);
            for (int i = 0; i < AveragePerColumn.Length; i++)
            {
                Console.Write(AveragePerColumn[i] + "\t");
            }
            Console.Write("\nAverage per row is:\n");
            double[] AveragePerRow = adapter.CalculateAveragePerRow(dataset);
            for (int i = 0; i < AveragePerRow.Length; i++)
            {
                Console.Write(AveragePerRow[i] + "\n");
            }
            Console.Write("\n");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_5_6_7
{
    class NotificationBuilder : IBuilder
    {
        public String Author = "Author";
        public String Title = "Title";
        public String Text = "Text";
        public DateTime Timestamp = DateTime.Now;
        public Category Level = Category.ERROR;
        public ConsoleColor Color = ConsoleColor.Gray;
        public ConsoleNotification Build()
        {
            return new ConsoleNotification(this.Author, this.Title, this.Text, this.Timestamp, this.Level, this.Color);
        }

        public IBuilder SetAuthor(string author) { this.Author = author; return this; }
        public IBuilder SetColor(ConsoleColor color) { this.Color = color; return this; }
        public IBuilder SetLevel(Category level) { this.Level = level; return this; }
        public IBuilder SetText(string text) { this.Text = text; return this; }
        public IBuilder SetTime(DateTime time) { this.Timestamp = time; return this; }
        public IBuilder SetTitle(string title) { this.Title = title; return this; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_4
{
    class Program
    {
        static void Main(string[] args)
        {
           
            ConsoleNotification notification = new ConsoleNotification("Windows Defender", "Virus", "You need to scan device", DateTime.Now, Category.ALERT, ConsoleColor.Red);
            NotificationManager manager = new NotificationManager();
            manager.Display(notification);
        }
    }
}

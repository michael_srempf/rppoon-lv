﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1
{
    class Program
    {
        //Potrebno je duboko kopiranje za ovu klasu zato što bi se metodom MemerwiseClone() kopirale reference na listu
        static void Main(string[] args)
        {
            Dataset data_csv = new Dataset("csv.csv");
            Console.WriteLine("Orginalni Dataset objekt:");
            data_csv.PrintOnConsole();
            Dataset copy = (Dataset)data_csv.Clone();
            Console.WriteLine("Kopija Dataset objekta:");
            copy.PrintOnConsole();
        }
    }
}

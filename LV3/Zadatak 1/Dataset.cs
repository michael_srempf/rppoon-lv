﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1
{
    class Dataset : Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }

        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }

        public void PrintOnConsole()
        {
            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < data[i].Count; j++)
                {
                    if (j + 1 == data[i].Count())
                    {
                        Console.Write(data[i][j]);
                    }
                    else
                    {
                        Console.Write(data[i][j] + ", ");
                    }

                }
                Console.WriteLine();
            }
        }
        public Prototype Clone()
        {
            Dataset copy = new Dataset();
            for (int i = 0; i < this.data.Count(); i++)
            {
                copy.data.Add(new List<string>());
                for (int j = 0; j < this.data[i].Count(); j++)
                {
                    copy.data[i].Add(this.data[i][j]);
                }
            }
            return (Prototype)copy;
        }
    }
}

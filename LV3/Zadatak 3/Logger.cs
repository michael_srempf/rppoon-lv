﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;
        private Logger()
        {
            this.filePath = "default.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void Log(string data)
        {
            using (System.IO.StreamWriter writer =
           new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(data);
            }
        }
        public void SetPath(string path)
        {
            this.filePath = path;
        }


    }
}

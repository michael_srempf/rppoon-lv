﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    //Zadatak 3: . Ako je datoteka postavljena na jednom mjestu u tekstu programa, uporabom loggera na
    //drugim mjestima u programu pisati će mo u istu datoteku, ako ne promijenimo gdje će se pisati
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = Logger.GetInstance();
            logger.Log("Test");
            logger.SetPath("Changed.txt");
            logger.Log("Test2");
        }
    }
}

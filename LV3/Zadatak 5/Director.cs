﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_5
{
    class Director
    {
        private IBuilder buileder;
        public Director(IBuilder Builder)
        {
            this.buileder = Builder;
        }
        public void ConstructAlert(string Author)
        {
            buileder.SetAuthor(Author).SetTitle("Scan device").SetText("you haven't scanned the device for a month").SetLevel(Category.ALERT).SetColor(ConsoleColor.Yellow);
        }
        public void ConstructError(string Author)
        {
            buileder.SetAuthor(Author).SetTitle("Error 404").SetText("404 Not Found").SetLevel(Category.ERROR).SetColor(ConsoleColor.Red);
        }
        public void ConstructInfo(string Author)
        {
            buileder.SetAuthor(Author).SetTitle("System update").SetText("System update is pending").SetLevel(Category.INFO).SetColor(ConsoleColor.Green);
        }

    }
}

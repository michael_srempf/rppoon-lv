﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_2
{
    class Program
    {
        //Zadatak 2 MatrixGenerator ima 2 odgovornosti:  brinuti o svom instanciranju i održavanju jedne instance
        //                                               pružati mogućnost stvaranja nove matirce
        static void Main(string[] args)
        {
            int rows, columns;
            rows = int.Parse(Console.ReadLine());
            columns = int.Parse(Console.ReadLine());
            MatrixGenerator generator=MatrixGenerator.GetInstance();
            double[,] matrix = generator.Get2DMatrix(rows,columns);
            
            for (int i = 0; i < (int)rows; i++)
            {
                for (int j = 0; j < (int)columns; j++)
                {
                    Console.Write(matrix[i,j]+"\t");
                }
                Console.WriteLine();
            }
        }
    }
}

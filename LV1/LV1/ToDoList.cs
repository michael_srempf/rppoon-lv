﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class ToDoList
    {
        private List<DatedNote> notes;

        public ToDoList()
        {
            notes = new List<DatedNote>();
        }
        public ToDoList(List<DatedNote> notes)
        {
            this.notes = notes;
        }
        public void AddNote(DatedNote note) { notes.Add(note);
            notes.OrderByDescending(x=> x.Importance).ThenBy(x=>x.time);
        }
        public void FinishImportant()
        {
            int MaxImportance = notes[0].Importance;
            notes.RemoveAll(note => note.Importance == MaxImportance);
        }
        public DatedNote getNote(int index)
        { 
            return notes[index]; 
        }
        public override string ToString()
        { StringBuilder stringBuilder = new StringBuilder();
            foreach(DatedNote note in notes)
            {
                stringBuilder.Append(note.ToString()).Append("\n\n");
            }
            return stringBuilder.ToString();
        }
    }
}

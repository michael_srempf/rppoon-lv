﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class DatedNote: Note
    {
        public DateTime time { get; set; }

        public DatedNote(String author,String text, int importance, DateTime time): base(author, text, importance)
        {
            this.time = time;
        }
        public DatedNote(String author, String text, DateTime time) : base(author, text)
        {
            this.time = time;
        }
        public DatedNote(String author, String text, int importance) : base(author, text, importance)
        {
            this.time = DateTime.Now;
        }
        public DatedNote(String author, String text) : base(author, text)
        {
            this.time = DateTime.Now;
        }
        public override string ToString()
        {
            return base.ToString() + "\nTime: " + time;
        }
    }
}

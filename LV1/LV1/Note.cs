﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Note
    {
        private String author;
        private String text;
        private int importance; //the larger the number, the more important

        public String getAuthor() { return author; }
        public String getText(){return text; }
        public int getImportance() { return importance; }
        public void setText(String text) { this.text = text; }
        public void setImportance(int importance) { this.importance = importance; }
        public Note()
        {
            this.author = "Anonymus";
            this.text = "not written";
            this.importance = 0;
        }
        public Note(String author, String text, int importance)
        {
            this.author = author;
            this.text = text;
            if (importance < 0) importance = 0;
            this.importance = importance;
        }
        public Note(String author, String text)
        {
            this.author = author;
            this.text = text;
            this.importance = 0;
        }

        public String Author
        {
            get{ return this.author; }
        }
        public String Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public int Importance
        {
            get { return this.importance; }
            set { this.importance = value; }
        }
        public override string ToString()
        {
            return "Author: " + this.author + "\nText: " + this.text;
        }
    }
}

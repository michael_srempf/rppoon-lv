﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note firstNote = new Note();
            Note secondNote = new Note("Marko Peric", "this is the second note", 1);
            Note thirdNote = new Note("Pero Peric", "this is the third note");
            Console.WriteLine("\t\t-----Zadatak 3-----");
            Console.WriteLine("Author: " + firstNote.getAuthor() + "\n text: " + firstNote.getText());
            Console.WriteLine("Author: " + secondNote.getAuthor() + "\n text: " + secondNote.getText());
            Console.WriteLine("Author: " + thirdNote.getAuthor() + "\n text: " + thirdNote.getText());
            Console.WriteLine("\t\t-----Zadatak 5-----");
            Console.WriteLine("\n"+secondNote.ToString());
            Console.WriteLine("\t\t-----Zadatak 6-----");
            DatedNote firstDatedNote = new DatedNote("Pero Peric", "this is dated note1",1);
            Console.WriteLine("\n"+firstDatedNote.ToString());
            Console.WriteLine("\t\t-----Zadatak 7-----");
            DatedNote secondDatedNote = new DatedNote("Marko Peric", "this is dated note2");
            DatedNote thirdDatedNote = new DatedNote("Darko Markovic", "this is dated note3",2);
            DatedNote fourthDatedNote = new DatedNote("Mislav Markovic", "this is dated note4",1);
            DatedNote fifthDatedNote = new DatedNote("Duje Peric", "this is dated note5",2);
            ToDoList tasks = new ToDoList();
            tasks.AddNote(firstDatedNote);
            tasks.AddNote(secondDatedNote);
            tasks.AddNote(thirdDatedNote);
            tasks.AddNote(fourthDatedNote);
            tasks.AddNote(fifthDatedNote);
            Console.WriteLine("\n\nNotes on to do list:");
            Console.WriteLine(tasks.ToString());
            tasks.FinishImportant();
            Console.WriteLine("\n\nNotes on to do list after finishing most important:");
            Console.WriteLine(tasks.ToString());

            Console.WriteLine("\n\nGet second note on list:\n" + tasks.getNote(1).ToString());
        }
    }
}

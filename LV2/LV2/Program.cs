﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
   

    class Program
    {

        //Zadatak 1
        //static void Main(string[] args)
        //{
        //    DiceRoller FirstDiceRoller = new DiceRoller();
        //    Die[] dice = new Die[20];
        //    for (int i = 0; i < dice.Length; i++)
        //    {
        //        dice[i] = new Die(6);
        //    }
        //    foreach (Die die in dice)
        //    {
        //        FirstDiceRoller.InsertDie(die);
        //    }
        //    FirstDiceRoller.RollAllDice();
        //    FirstDiceRoller.PrintOnConsole();
        //}
       

        //Zadatak 2
        //static void Main(string[] args)
        //{
        //    Random RandomGenerator = new Random();
        //    DiceRoller FirstDiceRoller = new DiceRoller();
        //    Die[] dice = new Die[20];
        //    for (int i = 0; i < dice.Length; i++)
        //    {
        //        dice[i] = new Die(6,RandomGenerator);
        //    }
        //    foreach (Die die in dice)
        //    {
        //        FirstDiceRoller.InsertDie(die);
        //    }
        //    FirstDiceRoller.RollAllDice();
        //    FirstDiceRoller.PrintOnConsole();
        //}

        //Zadatak 3
        //static void Main(string[] args)
        //{
        //    DiceRoller FirstDiceRoller = new DiceRoller();
        //    Die[] dice = new Die[20];
        //    for (int i = 0; i < dice.Length; i++)
        //    {
        //        dice[i] = new Die(6);
        //    }
        //    foreach (Die die in dice)
        //    {
        //        FirstDiceRoller.InsertDie(die);
        //    }
        //    FirstDiceRoller.RollAllDice();
        //    FirstDiceRoller.PrintOnConsole();
        //}

        //Zadatak 4
        //static void Main(string[] args)
        //{
        //    DiceRoller FirstDiceRoller = new DiceRoller();
        //    Die[] dice = new Die[20];
        //    for (int i = 0; i < dice.Length; i++)
        //    {
        //        dice[i] = new Die(6);
        //    }
        //    foreach (Die die in dice)
        //    {
        //        FirstDiceRoller.InsertDie(die);
        //    }
        //    FirstDiceRoller.RollAllDice();
        //    FirstDiceRoller.LogRollingResults();

        //    DiceRoller SecondDiceRoller = new DiceRoller("test.txt");
        //    foreach (Die die in dice)
        //    {
        //        SecondDiceRoller.InsertDie(die);
        //    }
        //    SecondDiceRoller.RollAllDice();
        //    SecondDiceRoller.LogRollingResults();
        //}


        //Zadatak 5
        static void Main(string[] args)
        {
            DiceRoller FirstDiceRoller = new DiceRoller();
            Die[] dice = new Die[20];
            for (int i = 0; i < dice.Length; i++)
            {
                dice[i] = new Die(6);
            }
            foreach (Die die in dice)
            {
                FirstDiceRoller.InsertDie(die);
            }
            FirstDiceRoller.RollAllDice();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            consoleLogger.Log(FirstDiceRoller);
            FileLogger fileLogger=new FileLogger("testFileLogger.txt");
            fileLogger.Log(FirstDiceRoller);
        }

    }
}

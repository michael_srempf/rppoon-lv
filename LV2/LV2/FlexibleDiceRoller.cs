﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    //Zadatak 6
    //class FlexibleDiceRoller: IDiceRoll, IDiceOperate
    //{
    //    private List<Die> dice;
    //    private List<int> resultForEachRoll;
    //    public FlexibleDiceRoller()
    //    {
    //        this.dice = new List<Die>();
    //        this.resultForEachRoll = new List<int>();
    //    }
    //    public void InsertDie(Die die)
    //    {
    //        dice.Add(die);
    //    }
    //    public void RemoveAllDice()
    //    {
    //        this.dice.Clear();
    //        this.resultForEachRoll.Clear();
    //    }
    //    public void RollAllDice()
    //    {
    //        this.resultForEachRoll.Clear();
    //        foreach (Die die in dice)
    //        {
    //            this.resultForEachRoll.Add(die.Roll());
    //        }
    //    }
    //}

    //Zadatak 7
    class FlexibleDiceRoller : IDiceRoll, IDiceOperate
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RemoveDiceWithSides(int sides)
        {
            dice.RemoveAll(x => x.GetNumberOfSides() == sides);
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
    }
}

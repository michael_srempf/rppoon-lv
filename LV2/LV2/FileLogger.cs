﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    //Zadatak 4
    //class FileLogger
    //{
    //    private string filePath;
    //    public FileLogger(string filePath)
    //    {
    //        this.filePath = filePath;
    //    }
    //    public void Log(string message)
    //    {
    //            using (System.IO.StreamWriter writer =
    //           new System.IO.StreamWriter(this.filePath,true))
    //            {
    //                writer.WriteLine(message);
    //            }      
    //    }

    //}

    //Zadatak 5
    class FileLogger:ILogger
    {
        private string filePath;
        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }
        public void Log(ILogable data)
        {
            using (System.IO.StreamWriter writer =
           new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(data.GetStringRepresentation());
            }
        }

    }
}

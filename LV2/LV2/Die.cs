﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    //Zadatak 1
    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;
    //    public Die(int numberOfSides)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = new Random();
    //    }
    //    public int Roll()
    //    {
    //        return randomGenerator.Next(1, numberOfSides + 1);
    //    }
    //}

    //Zadatak 2
    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;
    //    public Die(int numberOfSides,Random randomGenerator)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = randomGenerator;
    //    }
    //    public int Roll()
    //    {
    //        return randomGenerator.Next(1, numberOfSides + 1);
    //    }
    //}

    //Zadatak 3,4,5
    //class Die
    //{
    //    private int numberOfSides;
    //    private RandomGenerator randomGenerator;
    //    public Die(int numberOfSides)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = RandomGenerator.GetInstance();
    //    }
    //    public int Roll()
    //    {
    //        return randomGenerator.NextInt(1, numberOfSides + 1);

    //    }
    //}

    //Zadatak 7
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }
        public int Roll()
        {
            return randomGenerator.NextInt(1, numberOfSides + 1);

        }
        public int GetNumberOfSides()
        {
            return numberOfSides;
        }
    }
}

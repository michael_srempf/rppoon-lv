﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    //Zadatak 6
    //interface IDiceOperate
    //{
    //    void InsertDie(Die die);
    //    void RemoveAllDice();

    //}
    //Zadatak 7
    interface IDiceOperate
    {
        void InsertDie(Die die);
        void RemoveAllDice();
        void RemoveDiceWithSides(int sides);

    }
}

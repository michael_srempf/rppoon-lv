﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    //Zadatak 5
    interface ILogable
    {
        string GetStringRepresentation();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5i6i7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zad 6
            Console.WriteLine("Zadatak 6:");
            GroupNote note1 = new GroupNote("We have a work to do", new LightTheme());
            note1.AddMember("Michael");
            note1.AddMember("Peter");
            note1.AddMember("Josh");
            note1.Show();

            GroupNote note2 = new GroupNote("Deadline is tomorrow", new DarkTheme());
            note2.AddMember("Michael");
            note2.AddMember("Peter");
            note2.AddMember("Josh");
            note2.AddMember("Nicolas");
            note2.RemoveMemberAt(0);
            note2.RemoveMemberByName("Josh");
            note2.Show();

            //Zadatak 7
            Console.WriteLine("\n\nZadatak 7:");
            Notebook notebook = new Notebook(new LightTheme());
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.Display();
            notebook.ChangeTheme(new DarkTheme());
            notebook.Display();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5i6i7
{
    //Zad 6
    class GroupNote : Note
    {
        private List<string> group;
        public GroupNote(string message, ITheme theme) : base(message, theme) 
        {
            this.group = new List<string>();
        }
        public void AddMember(string name)
        {
            group.Add(name);
        }
        public void RemoveMemberAt(int index)
        {
            group.RemoveAt(index);
        }
        public void RemoveMemberByName(string name)
        {
            group.RemoveAll(p => p.Equals(name));
        }
        

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("REMINDER: ");
            foreach(string name in group)
            {
                Console.WriteLine(name);
            }
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            Console.ResetColor();
        }
    }
}

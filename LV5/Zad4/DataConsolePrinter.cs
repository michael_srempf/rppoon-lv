﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
namespace Zad4
{
    class DataConsolePrinter
    {
        public void Print(IDataset dataset)
        {
            try
            {
                ReadOnlyCollection<List<string>> data = new ReadOnlyCollection<List<string>>(dataset.GetData());
                if (data == null) return;
                foreach (List<string> row in data)
                {
                    foreach (string item in row)
                    {
                        Console.Write(item + " ");
                    }
                    Console.WriteLine();
                }
            }
            catch 
            { }
            
        }
    }
}

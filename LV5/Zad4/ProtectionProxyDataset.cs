﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
namespace Zad4
{
    class ProtectionProxyDataset : IDataset
    {
        private Dataset dataset;
        private List<int> allowedIDs;
        private ConsoleLogger logger;
        public User user { private get; set; }
        public ProtectionProxyDataset(User user)
        {
            this.allowedIDs = new List<int>(new int[] { 1, 3, 5 });
            this.user = user;
            logger = ConsoleLogger.getInstance();
        }
        private bool AuthenticateUser()
        {
            return allowedIDs.Contains(this.user.ID);
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            if (this.AuthenticateUser())
            {
                logger.Log(user, true);
                if (this.dataset == null)
                {
                    this.dataset = new Dataset("sensitiveData.csv");
                }
                return this.dataset.GetData();
            }
            else
            {
                logger.Log(user, false);
                return null;
            }
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
        private string filePath;
        private ConsoleLogger()
        {
           
            this.filePath = "DatasetAccesLog.txt";
        }
        public static ConsoleLogger getInstance()
        {
            if(instance==null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }
        public void Log(User user,bool AccesGranted) 
        {
            using (System.IO.StreamWriter writer =
           new System.IO.StreamWriter(this.filePath, true))
            {
                if (AccesGranted == true)
                {
                    writer.WriteLine("\nUser: " + user.Name);
                    writer.WriteLine("User ID: " + user.ID);
                    writer.WriteLine("Time: " + DateTime.Now);
                    writer.WriteLine("Acces: granted");
                }
                else
                {
                    writer.WriteLine("\nUser: " + user.Name);
                    writer.WriteLine("User ID: " + user.ID);
                    writer.WriteLine("Time: " + DateTime.Now);
                    writer.WriteLine("Acces: denied");
                }
            }
           
        }
        

    }
}

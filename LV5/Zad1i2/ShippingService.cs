﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1i2
{
    class ShippingService
    {
        private double PricePerKg;
        public ShippingService(double PricePerKg)
        {
            this.PricePerKg = PricePerKg;
        }
        public double ShippingPrice(IShipable packet)
        {
            return packet.Weight * PricePerKg;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1i2
{
    class Product : IShipable
    {
        public double Price { get; private set; }

        public double Weight { get; private set; }
        private string description;
        public Product(string description,double Price, double Weight)
        {
            this.description = description;
            this.Price = Price;
            this.Weight = Weight;
        }
        public string Description(int depth = 0)
        {
            return new string(' ', depth) + this.description;
        }
    }
}

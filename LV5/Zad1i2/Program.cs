﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1i2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product book = new Product("Harry Potter", 9.99, 1);
            Product ball = new Product("Ball", 25.60, 2.5);
            Box smallBox = new Box("Small box");
            smallBox.Add(book);
            Box bigBox = new Box("Big box");
            bigBox.Add(ball);
            bigBox.Add(smallBox);
            ShippingService shipping = new ShippingService(2.5);
            Console.WriteLine("Shipping price of big box is: " + shipping.ShippingPrice(bigBox));
            Console.WriteLine("Shipping price of book is: " + shipping.ShippingPrice(book));
        }
    }
}

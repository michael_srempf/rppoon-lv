﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("test.csv");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.Print(virtualProxyDataset);

            Console.WriteLine("\n\nProtection Proxy:");
            User admin = User.GenerateUser("admin");
            ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(admin);
            printer.Print(protectionProxyDataset);
            //Za drugog usera se ne ispisuju podaci zato što mu nije dopušten pristup
            User user = User.GenerateUser("common user");
            ProtectionProxyDataset protectionProxyDataset1 = new ProtectionProxyDataset(user);
            printer.Print(protectionProxyDataset1);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
namespace Zad3
{
    class DataConsolePrinter
    {
        public void Print(IDataset dataset)
        {
            
            if (dataset.GetData() == null) return;
            foreach(List<string> row in dataset.GetData())
            {
                foreach(string item in row)
                {
                    Console.Write(item+" ");
                }
                Console.WriteLine();
            }
        }
    }
}

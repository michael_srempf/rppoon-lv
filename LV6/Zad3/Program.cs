﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime today = DateTime.Now;
            CareTaker careTaker = new CareTaker();
            ToDoItem item = new ToDoItem("Meeting", "Meeting about XY", today.AddDays(3));
            Console.WriteLine(item.ToString());
            careTaker.PreviousState = item.StoreState();
            item.Rename("Party");
            item.ChangeTask("Birthday party");
            item.ChangeTimeDue(today.AddDays(3));
            Console.WriteLine(item.ToString());
            careTaker.PreviousState = item.StoreState();
            item.Rename("Dinner");
            item.ChangeTask("Dinner with friends");
            item.ChangeTimeDue(today.AddDays(1));
            Console.WriteLine(item.ToString());
            careTaker.PreviousState = item.StoreState();

            Console.WriteLine("\n\nRestoring previous states");
            item.RestoreState(careTaker.PreviousState);
            Console.WriteLine(item.ToString());
            item.RestoreState(careTaker.PreviousState);
            Console.WriteLine(item.ToString());
            item.RestoreState(careTaker.PreviousState);
            Console.WriteLine(item.ToString());
        }
    }
}

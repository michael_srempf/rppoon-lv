﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("Ball", 31.99));
            box.AddProduct(new Product("Shirt", 99.99));
            box.AddProduct(new Product("Shoes", 149.99));
            IAbstractIterator iterator = box.GetIterator();
            for(int i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    Console.WriteLine(iterator.First().ToString());


                }
                else
                {
                    Console.WriteLine(iterator.Next().ToString());
                }
            }
        }
    }
}

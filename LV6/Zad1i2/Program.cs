﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Fist note", "This is test1"));
            notebook.AddNote(new Note("Second note", "This is test2"));
            notebook.AddNote(new Note("Third note", "This is test3"));
            IAbstractIterator iterator = notebook.GetIterator();
           
           
            for(int i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    iterator.First().Show();
                    

                }
                else
                {
                    iterator.Next().Show();
                }
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zad 6
            StringLengthChecker stringLengthChecker = new StringLengthChecker(5);
            StringLowerCaseChecker stringLowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker stringUpperCaseChecker = new StringUpperCaseChecker();
            StringDigitChecker stringDigitChecker = new StringDigitChecker();
            stringLengthChecker.SetNext(stringLowerCaseChecker);
            stringLowerCaseChecker.SetNext(stringUpperCaseChecker);
            stringUpperCaseChecker.SetNext(stringDigitChecker);
            if (stringLengthChecker.Check("Test5") == true) Console.WriteLine("String Test5 je prosao test");
            else Console.WriteLine("String Test5 nije prosao test");

            if (stringLengthChecker.Check("TEST1") == true) Console.WriteLine("String TEST1 je prosao test");
            else Console.WriteLine("String TEST1 nije prosao test");

            //zad 7
            StringLengthChecker lengthChecker = new StringLengthChecker(8);
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            StringDigitChecker digitChecker = new StringDigitChecker();
            PasswordValidator passwordValidator = new PasswordValidator(lengthChecker);
            passwordValidator.AddChecker(lowerCaseChecker);
            passwordValidator.AddChecker(upperCaseChecker);
            passwordValidator.AddChecker(digitChecker);
            Console.WriteLine("\n\nPasswordValidator:");
            if (passwordValidator.Check("Passwor6")) Console.WriteLine("Passwor6 je prosao test");
            else Console.WriteLine("Passwor6 nije prosao test");

            if (passwordValidator.Check("password6")) Console.WriteLine("password6 je prosao test");
            else Console.WriteLine("password6 nije prosao test");
        }
    }
}

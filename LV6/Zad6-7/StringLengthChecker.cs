﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6_7
{
    class StringLengthChecker : StringChecker
    {
        private int lenght;
        public StringLengthChecker(int lenght) : base()
        {
            this.lenght = lenght;
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            return (stringToCheck.Length == this.lenght);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class CareTaker
    {
        private Stack<Memento> previousState = new Stack<Memento>();
        public Memento PreviousState { get { return previousState.Pop(); } set { previousState.Push(value); } }
    }
}

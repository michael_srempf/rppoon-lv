﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            BankAccount bankAccount = new BankAccount("Michael", "Osijek", 2000);
            Console.WriteLine(bankAccount.ToString());
            careTaker.PreviousState=bankAccount.StoreState();
            bankAccount.UpdateBalance(2500);
            Console.WriteLine(bankAccount.ToString());
            careTaker.PreviousState = bankAccount.StoreState();
            bankAccount.ChangeOwnerAddress("zagreb");
            Console.WriteLine(bankAccount.ToString());
            careTaker.PreviousState = bankAccount.StoreState();

            Console.WriteLine("\n\nRestoring previous states: ");
            bankAccount.RestoreChanges(careTaker.PreviousState);
            Console.WriteLine(bankAccount.ToString());
            bankAccount.RestoreChanges(careTaker.PreviousState);
            Console.WriteLine(bankAccount.ToString());
            bankAccount.RestoreChanges(careTaker.PreviousState);
            Console.WriteLine(bankAccount.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Memento
    {
        public string OwnerName { get; private set; }
        public string OwnerAdress { get; private set; }
        public decimal Balance { get; private set; }

        public Memento(string name, string adress, decimal balance)
        {
            this.OwnerName = name;
            this.OwnerAdress = adress;
            this.Balance = balance;
        }
    }
}
